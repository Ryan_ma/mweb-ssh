package com.mryan.webssh.service;

import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

/**
 * @Description: WebSSH的业务逻辑
 * @Author: MRyan
 * @Date: 2021/1/28
 */
public interface WebSSHService {
    /**
     * @Description: 初始化ssh连接
     * @Param: WebSocketSession
     * @return:
     */
    void initConnection(WebSocketSession session);

    /**
     * @Description: 处理客户段发的数据
     * @Param: String buffer, WebSocketSession session
     * @return:
     */
    void recvHandle(String buffer, WebSocketSession session);

    /**
     * @Description: 数据写回前端 for websocket
     * @Param: WebSocketSession session, byte[] buffer
     * @return:
     */
    void sendMessage(WebSocketSession session, byte[] buffer) throws IOException;

    /**
     * @Description: 关闭连接
     * @Param: WebSocketSession
     * @return:
     */
    void close(WebSocketSession session);
}
